import * as React from 'react';
import { StyleSheet, View, Text, Pressable } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './screens/HomeScreen';
import MenuOrganisateur from './screens/MenuOrganisateur';
import RoleScreen from './screens/RoleScreen';
import RegleScreen from './screens/RegleScreen';
import NewHunt from './screens/NewHunt';
import CategoryScreen from './screens/CategoryScreen';
import Gameplay from './screens/Gameplay';
import ManageHunt from './screens/ManageHunt';
import ManageUser from './screens/ManageUser';
import GererParcoursScreen from './screens/GererParcoursScreen';


const Stack = createNativeStackNavigator();

function App() {
    return (
        <NavigationContainer>
            {/* Option pour supprimer l'en-tête */}
            <Stack.Navigator screenOptions={{ headerShown: false }}> 
                <Stack.Screen name="Home" component={HomeScreen} />
                <Stack.Screen name="Role" component={RoleScreen} />
                <Stack.Screen name="Regle" component={RegleScreen} />
                <Stack.Screen name="MenuOrganisateur" component={MenuOrganisateur} />
                <Stack.Screen name="NewHunt" component={NewHunt} />
                <Stack.Screen name="GererParcoursScreen" component={GererParcoursScreen} />
                <Stack.Screen name="Category" component={CategoryScreen} />
                <Stack.Screen name="Gameplay" component={Gameplay} />
                <Stack.Screen name="ManageHunt" component={ManageHunt} />
                <Stack.Screen name="ManageUser" component={ManageUser} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}
export default App;