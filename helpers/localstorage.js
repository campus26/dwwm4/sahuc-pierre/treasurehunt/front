import AsyncStorage from '@react-native-async-storage/async-storage';

const getToken = async () => {
    try {
      const value = await AsyncStorage.getItem('token');
  
      if (value !== null) {
        // console.log('token from localstorage' +value);
  
        return value;
      }
    } catch (e) {
      console.log('Error reading token on localstorage : ' + e);
    }
  };

  const storeToken = async (value) => {
    try {
    //   console.log('store token : ' + value);
      await AsyncStorage.setItem('token', value);
    } catch (e) {
      console.log('Error storing token : ' + e);
    }
  };

  export { getToken, storeToken };