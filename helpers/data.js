export default class Quetes {
    constructor() {
        this.id = 1;
        this.name = "Quete 1";
        this.date_creation = "2021-01-01";
        this.temps = 3600;
        this.penalty = 300;
        this.auteur = "John Doe";
        this.markers = [
            {
                id: 1,
                lat: 48.866667,
                lng: 2.333333,
                question: "Quelle est la capitale de la France ?",
                reponse1: "Paris",
                reponse2: "Lyon",
                reponse3: "Nantes",
                correctAnswer: "Paris",
            },
            {
                id: 2,
                lat: 50.450001,
                lng: 30.523333,
                question: "Quelle est la capitale de l'Ukraine ?",
                reponse1: "Kiev",
                reponse2: "Odessa",
                reponse3: "Lviv",
                correctAnswer: "Kiev",
            }
        ];
    }
}