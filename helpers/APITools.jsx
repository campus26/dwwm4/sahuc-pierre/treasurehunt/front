import { getToken } from '../helpers/localstorage';

async function getAllHunt() {
    try {
        const token = await getToken();
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        };
        const response = await fetch('http://192.168.1.17:8000/api/quests', options);
        const jsonData = await response.json();
        console.log('Recupération des parcours : ' + JSON.stringify(jsonData));
        return jsonData;
    } catch (error) {
        console.log('GetData error : ' + error);
    }
}

async function getAllUser() {
    try {
        const token = await getToken();
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        };
        const response = await fetch('http://192.168.1.17:8000/api/users', options);
        const jsonData = await response.json();
        console.log('Recupération des users : ' + JSON.stringify(jsonData));
        return jsonData;
    } catch (error) {
        console.log('GetData error : ' + error);
    }
}

async function getHunt(id) {
    try {
        const token = await getToken();
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        };
        const response = await fetch('http://192.168.1.17:8000/api/quests/' + id + '', options);
        const jsonData = await response.json();
        console.log('Recupération du parcours ' + id + ' : ' + JSON.stringify(jsonData));
        return jsonData;
    } catch (error) {
        console.log('GetData error : ' + error);
    }
}

async function getUser(id) {
    try {
        const token = await getToken();
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        };
        const response = await fetch('http://192.168.1.17:8000/api/users/' + id + '', options);
        const jsonData = await response.json();
        console.log('Recupération du user ' + id + ' : ' + JSON.stringify(jsonData));
        return jsonData;
    } catch (error) {
        console.log('GetData error : ' + error);
    }
}

async function getAllMarkers() {
    try {
        const token = await getToken();
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        };
        const response = await fetch('http://192.168.1.17:8000/api/markers/', options);
        const jsonData = await response.json();
        console.log('Recupération de tous les markers : ' + JSON.stringify(jsonData));
        return jsonData;
    } catch (error) {
        console.log('GetData error : ' + error);
    }
}


export { getAllHunt, getAllUser, getHunt, getUser };