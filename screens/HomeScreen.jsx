import { useState, useEffect } from 'react';
import { StyleSheet, View, Text, Pressable, Image, TextInput } from 'react-native';
import * as Device from 'expo-device';
import * as Location from 'expo-location';
import MapView, { UrlTile } from 'react-native-maps';
import { styles } from '../styles/styles.js';
import { storeToken } from '../helpers/localstorage';

function HomeScreen({ navigation }) {

    const logo = require('../assets/img/logo.png');

    const [name, onChangeName] = useState('');
    const [username, onChangeEmail] = useState('');
    const [password, onChangePassword] = useState('');

    const [location, setLocation] = useState({
        latitude: 45.04288572553992,
        longitude: 3.8828942530702824,
        latitudeDelta: 0.009,
        longitudeDelta: 0.009,
    });
    const [locationMessage, setLocationMessage] = useState('Waiting...');
    const [errorMsg, setErrorMsg] = useState(null);

    useEffect(() => {
        (async () => {
            console.log('try to get location')
            let messageToDisplay = 'Waiting...';
            console.log('location message : ' + messageToDisplay);

            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                setErrorMsg('Permission to access location was denied');
                return;
            }

            let location = await Location.getCurrentPositionAsync({});
            console.log('location : ' + JSON.stringify(location));
            if (errorMsg) {
                messageToDisplay = errorMsg;
            } else if (location) {
                messageToDisplay = (JSON.stringify(location.coords.latitude) + ', ' + JSON.stringify(location.coords.longitude));
            } else {
                messageToDisplay = 'No location found';
            }
            console.log('messageToDisplay : ' + messageToDisplay);
            setLocation({
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
                latitudeDelta: 0.009,
                longitudeDelta: 0.009,
            });

            setLocationMessage(messageToDisplay);

        })();
    }, []);

    const [deviceType, setDeviceType] = useState('');
    useEffect(() => {
        // Obtient le modèle du périphérique
        const getDeviceType = async () => {
            let type = await Device.getDeviceTypeAsync();
            switch (type) {
                case 0:
                    type = 'un device inconnu';
                    break;
                case 1:
                    type = 'un téléphone';
                    break;
                case 2:
                    type = 'une tablette';
                    break;
                case 3:
                    type = 'un ordinateur de bureau';
                    break;
                case 4:
                    type = 'une TV';
                    break;
            };
            setDeviceType(type);
        };

        getDeviceType();
    }, []);

    async function getData(token) {
        try {
            const options = {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token,
                },
            };
            const response = await fetch('http://192.168.1.17:8000/api/markers', options);
            const json = await response.json();
            console.log('Recupération des markers : ' + JSON.stringify(json));
        } catch (error) {
            console.log('GetData error : ' + error);
        }
    }

    async function connect(token) {
        try {
            const options = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ username, password }),
            };
            const response = await fetch('http://192.168.1.17:8000/api/login_check', options);

            const data = await response.json();
            console.log('Acquisition du token : ' + JSON.stringify(data));

            if (data) {
                storeToken(data.token);
                // getData(token);
                navigation.navigate('Regle');
            }
        } catch (error) {
            console.log('Connect error : ' + error);
        }
    }

    return (
        <View style={styles.container}>
            <MapView mapType={"none"} style={stylesHome.map} region={location}>
                <UrlTile
                    urlTemplate="https://a.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png"
                    maximumZ={19}
                />
            </MapView>
            <View style={styles.containerLogo}>
                <Image source={logo} style={styles.logo}></Image>
            </View>
            <View style={styles.connectCard}>
                <View style={styles.contentCard}>
                    <Text style={styles.titre}>Connectez-vous pour participer</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={onChangeName}
                        value={name}
                        placeholder='Nom'
                    />
                    <TextInput
                        style={styles.input}
                        onChangeText={onChangeEmail}
                        value={username}
                        placeholder='Email'
                    />
                    <TextInput
                        style={styles.input}
                        onChangeText={onChangePassword}
                        value={password}
                        placeholder='mot de passe'
                    />
                    <Pressable style={styles.buttonRegister} onPress={() => navigation.navigate('Regle')}>
                        <Text style={styles.textButton}>S'enregistrer</Text>
                    </Pressable>
                    <Pressable style={styles.buttonLogin} onPress={connect}>
                        <Text style={styles.textButton}>Se connecter</Text>
                    </Pressable>
                </View>
            </View>
            <View>
                <Text style={styles.textFooter}>Vous utilisez {deviceType}</Text>
                <Text>Votre position est : {locationMessage}</Text>
            </View>
        </View>
    );
}
export default HomeScreen;

const stylesHome = StyleSheet.create({
    map: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
});