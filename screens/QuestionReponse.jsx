import React, { useState } from 'react';
import { View, Text, Pressable, StyleSheet, TextInput } from 'react-native';

function QuestionReponse({ markerCoordinate, saveMarkerAndData, closeModal }) {
    const [question, setQuestion] = useState('');
    const [reponses, setReponses] = useState(['', '', '']);
    const [correctReponse, setCorrectReponse] = useState(null);

    const handleReponseChange = (index, text) => {
        const newReponses = [...reponses];
        newReponses[index] = text;
        setReponses(newReponses);
    };

    const handleCorrectReponse = (index) => {
        setCorrectReponse(index);
    };

    return (
        <View style={styles.container}>
            <TextInput
                placeholder="Entrez votre question"
                style={styles.input}
                value={question}
                onChangeText={setQuestion}
            />
            {reponses.map((reponse, index) => (
                <View key={index} style={styles.reponseContainer}>
                    <TextInput
                        placeholder={`Entrez votre réponse ${index + 1}`}
                        style={styles.input}
                        value={reponse}
                        onChangeText={(text) => handleReponseChange(index, text)}
                    />
                    <Pressable
                        style={[
                            styles.reponseButton,
                            correctReponse === index && styles.correctReponse,
                            styles.squareButton
                        ]}
                        onPress={() => handleCorrectReponse(index)}
                    >
                        <Text>{correctReponse === index ? '✓' : ' '}</Text>
                    </Pressable>
                </View>
            ))}
            <Pressable onPress={() => saveMarkerAndData(question, reponses, correctReponse)} style={styles.saveButton}><Text style={styles.subButtonTexte}>Enregistrer</Text></Pressable>
            <Pressable onPress={closeModal} style={styles.closeButton}><Text>Fermer</Text></Pressable>
        </View>
    );
}

export default QuestionReponse;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
    },
    input: {
        width: '80%',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 20,
        paddingHorizontal: 10,
        borderRadius: 5,
    },
    reponseContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    reponseButton: {
        marginLeft: 10,
        backgroundColor: 'white',
        padding: 5,
        borderWidth: 1,
        borderColor: 'black',
        height: 40,
        justifyContent: 'center',
        marginBottom: 20,
    },
    correctReponse: {
        backgroundColor: 'green',
    },
    saveButton: {
        position: 'absolute',
        bottom: 20,
        left: 20,
        backgroundColor: '#1876D0',
        padding: 10,
        borderRadius: 30,
    },
    subButtonTexte: {
        color: 'white',
        textAlign: 'center',
    },
    closeButton: {
        position: 'absolute',
        top: 20,
        right: 20,
        backgroundColor: 'red',
        padding: 10,
        borderRadius: 30,
    },
    squareButton: {
        borderRadius: 0,
    },
});