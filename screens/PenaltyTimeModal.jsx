import React, { useState } from 'react';
import { Modal, StyleSheet, Text, TextInput, Pressable, View } from 'react-native';

function PenaltyTimeModal({ isVisible, onClose, onSetTime, selectedHours, selectedMinutes }) {
    const [hours, setHours] = useState('');
    const [minutes, setMinutes] = useState('');

    const handleSetTime = () => {
        const hoursInt = hours === '' ? 0 : parseInt(hours);
        const minutesInt = minutes === '' ? 0 : parseInt(minutes);

        if ((isNaN(hoursInt) || hoursInt < 0 || hoursInt > 23) && (isNaN(minutesInt) || minutesInt < 0 || minutesInt > 59)) {
            alert('Veuillez entrer une heure valide.');
            return;
        }

        if ((hoursInt > selectedHours) || (hoursInt === selectedHours && minutesInt > selectedMinutes)) {
            alert('La pénalité/le temps supplémentaire ne peut pas être supérieur au temps indiqué au chronomètre.');
            return;
        }

        onSetTime(hoursInt, minutesInt);
        onClose();
    };

    return (
        <Modal
            visible={isVisible}
            animationType="slide"
            transparent={true}
            onRequestClose={onClose}
        >
            <View style={styles.modalContainer}>
                <View style={styles.modalContent}>
                    <Text style={styles.modalTitle}>Pénalité/Prolongation</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.input}
                            placeholder="Heures"
                            onChangeText={text => setHours(text)}
                            keyboardType="numeric"
                        />
                        <TextInput
                            style={styles.input}
                            placeholder="Minutes"
                            onChangeText={text => setMinutes(text)}
                            keyboardType="numeric"
                        />
                    </View>
                    <Pressable style={styles.button} onPress={handleSetTime}>
                        <Text style={styles.buttonText}>Confirmer</Text>
                    </Pressable>
                </View>
            </View>
        </Modal>
    );
}

const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalContent: {
        backgroundColor: 'white',
        padding: 20,
        borderRadius: 10,
        alignItems: 'center',
    },
    modalTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    inputContainer: {
        flexDirection: 'row',
        marginBottom: 20,
    },
    input: {
        flex: 1,
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginRight: 10,
        paddingHorizontal: 10,
    },
    button: {
        backgroundColor: '#1876D0',
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 5,
    },
    buttonText: {
        color: 'white',
        fontWeight: 'bold',
    },
});

export default PenaltyTimeModal;