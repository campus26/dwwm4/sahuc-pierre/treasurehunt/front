import React from 'react';
import { ImageBackground, StyleSheet, View, Text, Pressable, Dimensions } from 'react-native';

function MenuOrganisateur({ navigation }) {
    const back = require('../assets/img/kiev.png');

    return (
        <View style={styles.container}>
            <ImageBackground source={back} resizeMode="cover" style={styles.imageBack} />
            <View style={styles.contentCard}>
                <View style={styles.titleCard}>
                    <Text style={styles.title}>Menu Organisateur</Text>
                </View>
                <View style={styles.roleCard}>
                    <Pressable onPress={() => navigation.navigate('NewHunt')}>
                        <Text style={styles.subTitle}>Nouvelle chasse au trésor</Text>
                    </Pressable>
                </View>
                <View style={styles.roleCard}>
                <Pressable onPress={() => navigation.navigate('ManageHunt')}>
                    <Text style={styles.subTitle}>Gérer les parcours</Text>
                </Pressable>
                </View>
                <View style={styles.roleCard}>
                <Pressable onPress={() => navigation.navigate('ManageUser')}>
                    <Text style={styles.subTitle}>Gérer les utilisateurs</Text>
                </Pressable>
                </View>
            </View>
        </View>
    );
}

export default MenuOrganisateur;

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const contentCardHeight = screenHeight / 1.2;
const contentCardWidth = screenWidth * 0.9;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageBack: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    contentCard: {
        position: 'absolute',
        width: contentCardWidth,
        height: "93%",
        top: '45%',
        marginTop: -contentCardHeight / 2,
        alignItems: 'center',
    },
    titleCard: {
        marginBottom: 50,
        backgroundColor: 'white',
        opacity: 0.8,
        borderRadius: 30,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 38,
        fontWeight: 'bold',
        padding: 10,
        color: '#000AFF',
        textAlign: 'center',
        textShadowColor: 'rgba(0, 0, 0, 0.5)',
        textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 4,
    },
    roleCard: {
        marginVertical: 10,
        backgroundColor: 'white',
        opacity: 0.8,
        borderRadius: 30,
        width: '100%',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
        paddingBottom: 10,
    },
    subTitle: {
        textAlign: 'center',
        color: '#FF0000',
        fontSize: 26,
        fontWeight: 'bold',
    },
});