import React, { useState } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { RadioButton } from 'react-native-paper';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    padding: 15,
  },
  textButton: {
    fontSize: 20,
    textAlign: "center",
    color: '#FF0000',
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
  textQuestion: {
    fontSize: 18,
    paddingLeft: 15,
    paddingRight: 10,
    paddingBottom: 10,
    fontWeight: 'bold',
  },
  labelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 5,
  },
  textAnswer: {
    textAlign: 'left',
    fontSize: 18,
  },
  subButton: {
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#1876D0',
    borderRadius: 30,
    width: '50%',
    height: "12%",
    justifyContent: 'center',
    alignSelf: 'center',
  },
  subButtonTexte: {
    color: 'white',
    textAlign: 'center',
  },
});

const Question = ({ question, reponse1, reponse2, reponse3, penalty, onNextMarker, onCorrectAnswer, setCurrentTime, correctAnswer }) => {
  const [selectedAnswer, setSelectedAnswer] = useState(null);
  const [showContent, setShowContent] = useState(false);

  function handleAnswer() {
    if (selectedAnswer === correctAnswer) {
      onCorrectAnswer();
      setCurrentTime(prevTime => prevTime + penalty);
      onNextMarker();
    } else {
      setCurrentTime(prevTime => prevTime - penalty);
    }
  }

  return (
    <View style={styles.container}>
      {!showContent && (
        <TouchableOpacity onPress={() => setShowContent(true)}>
          <View><Text style={styles.textButton}>Question</Text></View>
        </TouchableOpacity>
      )}
      {showContent && (
        <>
          <TouchableOpacity onPress={() => setShowContent(false)}>
            <View><Text style={styles.textButton}>Question</Text></View>
          </TouchableOpacity>
          <Text style={styles.textQuestion}>{question}</Text>
          <TouchableOpacity style={styles.labelContainer} onPress={() => setSelectedAnswer(reponse1)}>
            <RadioButton
              value={reponse1}
              status={selectedAnswer === reponse1 ? 'checked' : 'unchecked'}
              color="#000AFF"
              style={styles.radioButton}
            />
            <View style={{ flex: 1 }}>
              <Text style={styles.textAnswer}>{reponse1}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.labelContainer} onPress={() => setSelectedAnswer(reponse2)}>
            <RadioButton
              value={reponse2}
              status={selectedAnswer === reponse2 ? 'checked' : 'unchecked'}
              color="#000AFF"
              style={styles.radioButton}
            />
            <View style={{ flex: 1 }}>
              <Text style={styles.textAnswer}>{reponse2}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.labelContainer} onPress={() => setSelectedAnswer(reponse3)}>
            <RadioButton
              value={reponse3}
              status={selectedAnswer === reponse3 ? 'checked' : 'unchecked'}
              color="#000AFF"
              style={styles.radioButton}
            />
            <View style={{ flex: 1 }}>
              <Text style={styles.textAnswer}>{reponse3}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.subButton} onPress={handleAnswer}>
            <Text style={styles.subButtonTexte}>Répondre</Text>
          </TouchableOpacity>
        </>
      )}
    </View>
  );
};

export default Question;