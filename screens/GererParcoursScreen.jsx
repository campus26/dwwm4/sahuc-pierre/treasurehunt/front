import React, { useState } from 'react';
import { ImageBackground, StyleSheet, View, Text, Pressable, Dimensions, TouchableOpacity } from 'react-native';
import { RadioButton } from 'react-native-paper';
import { useRoute } from '@react-navigation/native';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const contentCardHeight = screenHeight / 1.2;
const contentCardWidth = screenWidth * 0.9;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  imageBack: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  contentCard: {
    position: 'absolute',
    width: contentCardWidth,
    height: "93%",
    top: '45%',
    marginTop: -contentCardHeight / 2,
    alignItems: 'center',
  },
  titleCard: {
    marginBottom: 50,
    backgroundColor: 'white',
    opacity: 0.8,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  title: {
    fontSize: 38,
    fontWeight: 'bold',
    padding: 10,
    color: '#000AFF',
    textAlign: 'center',
    textShadowColor: 'rgba(0, 0, 0, 0.5)',
    textShadowOffset: { width: 2, height: 2 },
    textShadowRadius: 4,
  },
  roleCard: {
    marginVertical: 10,
    backgroundColor: 'white',
    opacity: 0.8,
    borderRadius: 30,
    width: '100%',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10, 
  },
  labelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 5,
  },
  subTitle: {
    textAlign: 'center',
    color: '#FF0000',
    fontSize: 26,
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
  textContent: {
    fontSize: 16,
    paddingLeft: 15,
    paddingBottom: 10,
  },
  buttonContainer: {
    marginTop: 20,
    backgroundColor: '#1876D0',
    borderRadius: 30,
    width: '70%',
    height: "8%",
    justifyContent: 'center',
    marginTop: 'auto',
    alignSelf: 'center',
  },
  texte: {
    color: 'white',
    fontSize: 26,
    textAlign: 'center',
  },
});

function RadioButtonWithLabel({ label, selectedGame, setSelectedGame }) {
  return (
    <TouchableOpacity style={styles.labelContainer} onPress={() => setSelectedGame(label)}>
      <RadioButton
        value={label}
        status={selectedGame === label ? 'checked' : 'unchecked'}
        onPress={() => setSelectedGame(label)}
        color="#000AFF"
        style={styles.radioButton}
      />
      <View style={{ flex: 1 }}>
        <Text style={styles.subTitle}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
}

function GererParcoursScreen({ navigation }) {
  const back = require('../assets/img/kiev.png');
  const route = useRoute();
  const [selectedGame, setSelectedGame] = useState('');

  const { name, markerCounter, selectedTimeInSeconds, selectedPenaltyHours, selectedPenaltyMinutes } = route.params || {};

  const hasData = name !== undefined || markerCounter !== undefined || selectedTimeInSeconds !== undefined || selectedPenaltyHours !== undefined || selectedPenaltyMinutes !== undefined;

  if (!hasData) {
    return (
      <View style={styles.container}>
        <ImageBackground source={back} resizeMode="cover" style={styles.imageBack} />
        <View style={styles.contentCard}>
          <View style={styles.titleCard}>
            <Text style={styles.title}>Vos quêtes</Text>
          </View>
          <View style={styles.roleCard}>
          <Text style={styles.textContent}>Aucune quête disponible</Text>
          </View>
          <Pressable style={styles.buttonContainer} onPress={() => navigation.navigate('NewHunt')}>
            <Text style={[styles.texte, { fontSize: 18 }]}>Nouvelle chasse au trésor</Text>
          </Pressable>
        </View>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <ImageBackground source={back} resizeMode="cover" style={styles.imageBack} />
      <View style={styles.contentCard}>
        <View style={styles.titleCard}>
          <Text style={styles.title}>Vos quêtes</Text>
        </View>
        <View style={styles.roleCard}>
          <RadioButtonWithLabel
            label={name}
            selectedGame={selectedGame}
            setSelectedGame={setSelectedGame}
          />
          <Text style={styles.textContent}>Temps : <Text style={{ color: '#000AFF', textDecorationLine: 'underline' }}>{Math.floor(selectedTimeInSeconds / 3600)} h {Math.floor((selectedTimeInSeconds % 3600) / 60)} m</Text></Text>
          <Text style={styles.textContent}>Temps additionnel : <Text style={{ color: '#000AFF', textDecorationLine: 'underline' }}>{selectedPenaltyHours} h {selectedPenaltyMinutes} m</Text></Text>
          <Text style={styles.textContent}>Nombre de marqueurs: <Text style={{ color: '#FF0000', fontWeight: 'bold' , textDecorationLine: 'underline'}}>{markerCounter}</Text></Text>
        </View>
        <Pressable style={styles.buttonContainer}>
          <Text style={styles.texte}>Modifier</Text>
        </Pressable>
      </View>
    </View>
  );
}

export default GererParcoursScreen;