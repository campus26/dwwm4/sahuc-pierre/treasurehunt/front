import React, { useState, useEffect } from 'react';
import { ImageBackground, ScrollView, StyleSheet, View, Text, Pressable, Dimensions, TextInput, KeyboardAvoidingView, Platform, Keyboard, Modal } from 'react-native';
import TimerModal from './TimerModal';
import PenaltyTimeModal from './PenaltyTimeModal';
import Hunt from './Hunt';

function NewHunt({ navigation, route }) {
    const [newParcoursName, setNewParcoursName] = useState('');
    const [markerCoordinates, setMarkerCoordinates] = useState([]);
    const [keyboardHeight, setKeyboardHeight] = useState(0);
    const [isKeyboardVisible, setKeyboardVisible] = useState(false);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [markerCounter, setMarkerCounter] = useState(0);
    const maxMarkers = 10;
    const [isTimerModalVisible, setIsTimerModalVisible] = useState(false);
    const [selectedTimeInSeconds, setSelectedTimeInSeconds] = useState(0);
    const [isPenaltyTimeModalVisible, setIsPenaltyTimeModalVisible] = useState(false);
    const [selectedPenaltyHours, setSelectedPenaltyHours] = useState(0);
    const [selectedPenaltyMinutes, setSelectedPenaltyMinutes] = useState(0);

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', e => {
            setKeyboardHeight(e.endCoordinates.height);
            setKeyboardVisible(true);
        });
        const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
            setKeyboardHeight(0);
            setKeyboardVisible(false);
        });

        return () => {
            keyboardDidShowListener.remove();
            keyboardDidHideListener.remove();
        };
    }, []);

    useEffect(() => {
        if (route.params && route.params.markerCoordinate) {
            if (markerCounter < maxMarkers) {
                setIsModalVisible(true);
                setMarkerCoordinates([...markerCoordinates, route.params.markerCoordinate]);
                setMarkerCounter(markerCounter + 1);
            }
        }
    }, [route.params]);

    const closeModal = () => {
        setIsModalVisible(false);
    };

    const openTimerModal = () => {
        setIsTimerModalVisible(true);
    };
    
    const closeTimerModal = () => {
        setIsTimerModalVisible(false);
    };
    
    const handleSetTime = (hours, minutes) => {
        const timeInSeconds = hours * 3600 + minutes * 60;
        setSelectedTimeInSeconds(timeInSeconds);
    };

    const saveMarker = (markerCoordinate) => {
        if (markerCounter < maxMarkers) {
            setMarkerCoordinates([...markerCoordinates, markerCoordinate]);
            setMarkerCounter(markerCounter + 1);
        }
        setIsModalVisible(false);
    };

    const openPenaltyTimeModal = () => {
        setIsPenaltyTimeModalVisible(true);
    };

    const closePenaltyTimeModal = () => {
        setIsPenaltyTimeModalVisible(false);
    };

    const handleSetPenaltyTime = (hours, minutes) => {
        if (hours * 3600 + minutes * 60 > selectedTimeInSeconds) {
            alert('La pénalité/le temps supplémentaire ne peut pas être supérieur au temps indiqué au chronomètre.');
            return;
        }
        setSelectedPenaltyHours(hours);
        setSelectedPenaltyMinutes(minutes);
        closePenaltyTimeModal();
    };

    const createParcours = () => {
        if (newParcoursName.trim() === '') {
            alert('Veuillez entrer un nom pour le parcours.');
            return;
        }
        if (markerCounter === 0) {
            alert('Veuillez ajouter au moins un marqueur.');
            return;
        }
        if (selectedTimeInSeconds === 0) {
            alert('Veuillez régler la minuterie.');
            return;
        }
        if (selectedPenaltyHours === 0 && selectedPenaltyMinutes === 0) {
            alert('Veuillez fixer une pénalité/prolongation.');
            return;
        }

        navigation.navigate('GererParcoursScreen', {
            markerCoordinates: markerCoordinates,
            name: newParcoursName,
            markerCounter: markerCounter,
            selectedTimeInSeconds: selectedTimeInSeconds,
            selectedPenaltyHours: selectedPenaltyHours,
            selectedPenaltyMinutes: selectedPenaltyMinutes
        });
    };

    return (
        <View style={styles.container}>
            <ImageBackground source={require('../assets/img/kiev.png')} resizeMode="cover" style={styles.imageBack} />
            <KeyboardAvoidingView style={styles.contentCard} behavior={Platform.OS === "ios" ? "padding" : "height"}>
                <View style={[styles.titleCard, isKeyboardVisible && { marginVertical: 160 - keyboardHeight / 10 }]}>
                    <Text style={styles.title}>Menu Organisateur</Text>
                </View>
                <ScrollView contentContainerStyle={styles.scrollViewContent}>
                    <View style={styles.roleCard}>
                        <Text style={styles.subTitle}>Nouvelle Chasse au trésor</Text>
                        <TextInput
                            onChangeText={setNewParcoursName}
                            value={newParcoursName}
                            placeholder='Donnez un nom au parcours'
                            style={styles.input}
                        />
                        {markerCounter > 0 && Array.from({ length: markerCounter }, (_, index) => <Text key={index}>Marker {index + 1}</Text>)}
                        {markerCounter < maxMarkers && <Pressable onPress={() => setIsModalVisible(true)} style={styles.subButton}><Text style={styles.subButtonTexte}>Ajouter un marker</Text></Pressable>}
                        <Text>Minuteur : {Math.floor(selectedTimeInSeconds / 3600)} heures {Math.floor((selectedTimeInSeconds % 3600) / 60)} minutes</Text>
                        <Pressable onPress={openTimerModal} style={styles.subButton}><Text style={styles.subButtonTexte}>Définir le temps imparti</Text></Pressable>
                        <TimerModal 
                            isVisible={isTimerModalVisible} 
                            onClose={closeTimerModal} 
                            onSetTime={handleSetTime} 
                        />
                        <Text>Pénalité/Prolongation : {selectedPenaltyHours} heures {selectedPenaltyMinutes} minutes</Text>
                        <Pressable onPress={openPenaltyTimeModal} style={styles.subButton}><Text style={styles.subButtonTexte}>Fixer une pénalité/prolongation</Text></Pressable>
                        <PenaltyTimeModal
                            isVisible={isPenaltyTimeModalVisible}
                            onClose={closePenaltyTimeModal}
                            onSetTime={handleSetPenaltyTime}
                        />
                    </View>
                </ScrollView>
                <Pressable 
                    style={[styles.buttonContainer, isKeyboardVisible && { marginVertical: - keyboardHeight }]}
                    onPress={createParcours}
                    disabled={!newParcoursName.trim() || markerCounter === 0 || selectedTimeInSeconds === 0}
                >
                    <Text style={styles.texte}>Créer le parcours</Text>
                </Pressable>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={isModalVisible}
                    onRequestClose={() => setIsModalVisible(false)}
                >
                    <Hunt
                        name={newParcoursName}
                        closeModal={closeModal}
                        saveMarker={saveMarker}
                    />
                </Modal>
            </KeyboardAvoidingView>
        </View>
    );
}

export default NewHunt;

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const contentCardHeight = screenHeight / 1.2;
const contentCardWidth = screenWidth * 0.9;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageBack: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    contentCard: {
        position: 'absolute',
        width: contentCardWidth,
        height: "93%",
        top: '45%',
        marginTop: -contentCardHeight / 2,
        alignItems: 'center',
    },
    titleCard: {
        marginBottom: 50,
        backgroundColor: 'white',
        opacity: 0.8,
        borderRadius: 30,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 38,
        fontWeight: 'bold',
        padding: 10,
        color: '#000AFF',
        textAlign: 'center',
        textShadowColor: 'rgba(0, 0, 0, 0.5)',
        textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 4,
    },
    scrollViewContent: {
        flexGrow: 1,
        paddingBottom: 20,
    },
    roleCard: {
        marginVertical: 10,
        backgroundColor: 'white',
        opacity: 0.8,
        borderRadius: 30,
        width: '100%',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
        alignItems: 'stretch',
    },
    subTitle: {
        textAlign: 'center',
        color: '#FF0000',
        fontSize: 26,
        fontWeight: 'bold',
        textDecorationLine: 'underline',
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 10,
        marginTop: 10,
        paddingHorizontal: 10,
    },
    buttonContainer: {
        backgroundColor: '#1876D0',
        borderRadius: 30,
        width: '70%',
        height: "8%",
        justifyContent: 'center',
        marginTop: 'auto',
        alignSelf: 'center',
    },
    subButton: {
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#1876D0',
        borderRadius: 30,
        width: '80%',
        height: "12%",
        justifyContent: 'center',
        alignSelf: 'center',
    },
    texte: {
        color: 'white',
        fontSize: 26,
        textAlign: 'center',
    },
    subButtonTexte: {
        color: 'white',
        fontSize: 16,
        textAlign: 'center',
    },
});