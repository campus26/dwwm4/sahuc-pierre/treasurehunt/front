import React, { useState, useEffect } from 'react'; 
import { ScrollView, View, StyleSheet, Dimensions, Text, Pressable, ImageBackground } from 'react-native';
import { getAllUser } from '../helpers/APITools';

function ManageUser({ navigation }) {
    const back = require('../assets/img/kiev.png');
    const [userList, setuserList] = useState([]);

    useEffect(() => {
        async function fetchUser() {
          const data = await getAllUser();
          setuserList(data);
        }
        fetchUser();
      }, []);

    return (
        
        <ScrollView contentContainerStyle={styles.container}>
            <ImageBackground source={back} resizeMode="cover" style={styles.imageBack} />
            <View style={styles.contentCard}>
                    <Text style={styles.title}>Liste des joueurs</Text>
                    {/* <Pressable onPress={getData}><Text>get data</Text></Pressable> */}
            </View>
            <ScrollView style={styles.scrollViewContainer}>
                {userList.map((item, index) => (
                    <View key={index}>
                        <Text style={styles.subTitle}>{item.email}</Text>
                        <Text style={styles.textContent}>Roles : {item.roles}</Text>
                    </View>
                ))}
            </ScrollView>
        </ScrollView>
    );}
export default ManageUser;

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const contentCardHeight = screenHeight / 1.2;
const contentCardWidth = screenWidth * 0.9;

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center'
      },
    imageBack: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    contentCard: {
        position: 'absolute',
        width: contentCardWidth,
        height: "93%",
        top: '45%',
        marginTop: -contentCardHeight / 2,
        alignItems: 'center',
    },
    titleCard: {
        marginBottom: 50,
        backgroundColor: 'white',
        opacity: 0.8,
        borderRadius: 30,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
      },
    title: {
        fontSize: 38,
        fontWeight: 'bold',
        padding: 10,
        color: '#000AFF',
        textAlign: 'center',
        textShadowColor: 'rgba(0, 0, 0, 0.5)',
        textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 4,
      },
      roleCard: {
        marginVertical: 10,
        backgroundColor: 'white',
        opacity: 0.8,
        borderRadius: 30,
        width: '100%',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10, 
        paddingBottom: 10,
      },
      subTitle: {
        textAlign: 'center',
        color: '#FF0000',
        fontSize: 26,
        fontWeight: 'bold',
      },
      textContent: {
        fontSize: 14,
        paddingLeft: 35,
      },
      scrollViewContainer: {
        backgroundColor: '#FFF',
        padding: 20,
        position: 'absolute',
        width: contentCardWidth,
        height: "93%",
        top: '60%',
        marginTop: -contentCardHeight / 2,
      },
});
