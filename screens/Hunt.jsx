import React, { useState } from 'react';
import { View, Text, Pressable, StyleSheet, Modal } from 'react-native';
import MapView, { Marker, UrlTile } from 'react-native-maps';
import QuestionReponse from './QuestionReponse';

function Hunt({ closeModal, saveMarker }) {
    const [markerCoordinate, setMarkerCoordinate] = useState(null);
    const [isSecondModalVisible, setIsSecondModalVisible] = useState(false);
    const [questionsAndAnswers, setQuestionsAndAnswers] = useState([]);

    const handleMapPress = (event) => {
        const { coordinate } = event.nativeEvent;
        setMarkerCoordinate(coordinate);
        setIsSecondModalVisible(true);
    };

    const saveMarkerAndData = (question, reponses, correctReponse) => {
        saveMarker(markerCoordinate);
        setQuestionsAndAnswers([...questionsAndAnswers, { question, reponses, correctReponse }]);
        closeModal();
    };

    return (
        <View style={styles.container}>
            <MapView
                mapType={"none"}
                style={styles.map}
                onPress={handleMapPress}
            >
                <UrlTile
                    urlTemplate="https://a.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png"
                    maximumZ={19}
                />
                {markerCoordinate && <Marker coordinate={markerCoordinate} />}
            </MapView>
            <Pressable onPress={closeModal} style={styles.closeButton}><Text>Fermer</Text></Pressable>
            <Modal
                animationType="slide"
                transparent={true}
                visible={isSecondModalVisible}
                onRequestClose={() => setIsSecondModalVisible(false)}
            >
                <QuestionReponse
                    markerCoordinate={markerCoordinate}
                    saveMarkerAndData={saveMarkerAndData}
                    closeModal={() => setIsSecondModalVisible(false)}
                />
            </Modal>
        </View>
    );
}

export default Hunt;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    map: {
        flex: 1,
    },
    closeButton: {
        position: 'absolute',
        top: 20,
        right: 20,
        backgroundColor: 'red',
        padding: 10,
        borderRadius: 30,
    },
});