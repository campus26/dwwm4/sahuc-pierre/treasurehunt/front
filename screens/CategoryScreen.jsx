import React, {useState, useEffect} from 'react';
import { StyleSheet, View, Text, Pressable, ImageBackground, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import { RadioButton } from 'react-native-paper';
import { getAllHunt } from '../helpers/APITools';
import Quetes from '../helpers/data';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const contentCardHeight = screenHeight / 1.2;
const contentCardWidth = screenWidth * 0.9;

function RadioButtonWithLabel({ label, value, selectedGame, setSelectedGame }) {
  return (
    <TouchableOpacity style={styles.labelContainer} onPress={() => setSelectedGame(value)}>
      <RadioButton
        value={value}
        status={selectedGame === value ? 'checked' : 'unchecked'}
        onPress={() => setSelectedGame(value)}
        color="#000AFF"
        style={styles.radioButton}
      />
      <View style={{ flex: 1 }}>
        <Text style={styles.subTitle}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
}


const formatTime = (seconds) => {
  const hours = Math.floor(seconds / 3600);
  const minutes = Math.floor((seconds % 3600) / 60);
  const remainingSeconds = seconds % 60;
  return `${hours}:${minutes}:${remainingSeconds}`;
};

function CategoryScreen({ navigation }) {
  const back = require('../assets/img/kiev.png');
  const [selectedGame, setSelectedGame] = useState('');

  const isGameSelected = () => {
    return selectedGame !== '';
  };

  const quetes = new Quetes();

  const [huntList, setHuntList] = useState([]);

    useEffect(() => {
        async function fetchHunt() {
          const data = await getAllHunt();
          setHuntList(data);
        }
        fetchHunt();
      }, []);

  return (
    <View style={styles.container}>
      <ImageBackground source={back} resizeMode="cover" style={styles.imageBack} />
      <View style={styles.contentCard}>
        <View style={styles.titleCard}>
          <Text style={styles.title}>Choisissez votre quete</Text>
        </View>
        <ScrollView contentContainerStyle={styles.scrollViewContainer}>
        <View style={styles.roleCard}>
        {huntList.map((item, index) => (
            <View key={index}>
              <RadioButtonWithLabel
                label={item.name}
                value={index}
                selectedGame={selectedGame}
                setSelectedGame={setSelectedGame}
              />
              <Text style={styles.textContent}>Créé par: <Text style={{ color: '#000AFF', textDecorationLine: 'underline' }}>{item.userId.email}</Text> {quetes.date_creation}</Text>
              <Text style={styles.textContent}>Temps: <Text style={{ color: '#000AFF', textDecorationLine: 'underline' }}>{formatTime(item.duration)}</Text></Text>
              <Text style={styles.textContent}>Penalty: <Text style={{ color: '#000AFF', textDecorationLine: 'underline' }}>{formatTime(quetes.penalty)}</Text></Text>
              <Text style={styles.textContent}>Nombre de marqueurs: <Text style={{ color: '#FF0000', fontWeight: 'bold' , textDecorationLine: 'underline'}}>{item.markers.length}</Text></Text>
            </View>
        ))}
        </View>
        </ScrollView>
        <Pressable r
          onPress={() => {
            if (isGameSelected()) {
              navigation.navigate('Gameplay', { marker: huntList[selectedGame].markers, temps: huntList[selectedGame].duration, penalty: quetes.penalty });
            }
          }} 
          style={[styles.buttonContainer, { opacity: isGameSelected() ? 1 : 0.5 }]}
          disabled={!isGameSelected()}
        >
          <Text style={styles.texte}>Continuer</Text>
        </Pressable>
      </View>
    </View>
  );
}

export default CategoryScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  imageBack: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  contentCard: {
    position: 'absolute',
    width: contentCardWidth,
    height: "93%",
    top: '45%',
    marginTop: -contentCardHeight / 2,
    alignItems: 'center',
  },
  titleCard: {
    marginBottom: 50,
    backgroundColor: 'white',
    opacity: 0.8,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 38,
    fontWeight: 'bold',
    padding: 10,
    color: '#000AFF',
    textAlign: 'center',
    textShadowColor: 'rgba(0, 0, 0, 0.5)',
    textShadowOffset: { width: 2, height: 2 },
    textShadowRadius: 4,
  },
  roleCard: {
    marginVertical: 10,
    backgroundColor: 'white',
    opacity: 0.8,
    borderRadius: 30,
    width: '100%',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 5, 
  },
  labelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 5,
  },
  subTitle: {
    textAlign: 'center',
    color: '#FF0000',
    fontSize: 26,
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
  textContent: {
    fontSize: 16,
    paddingLeft: 15,
    paddingBottom: 7,
  },
  buttonContainer: {
    marginTop: 20,
    backgroundColor: '#1876D0',
    borderRadius: 30,
    width: '70%',
    height: "8%",
    justifyContent: 'center',
    marginTop: 'auto',
    alignSelf: 'center',
  },
  texte: {
    color: 'white',
    fontSize: 26,
    textAlign: 'center',
  },
});