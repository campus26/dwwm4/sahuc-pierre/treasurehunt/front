import * as React from 'react';
import { StyleSheet, View, Text, Pressable, ImageBackground, Dimensions, Image, ScrollView } from 'react-native';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const contentCardHeight = screenHeight / 1.2;
const contentCardWidth = screenWidth * 0.9;

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  imageBack: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  contentCard: {
    position: 'absolute',
    width: contentCardWidth,
    height: "93%",
    top: '45%',
    marginTop: -contentCardHeight / 2,
    alignItems: 'center',
  },
  titleCard: {
    marginBottom: 50,
    backgroundColor: 'white',
    opacity: 0.8,
    borderRadius: 30,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 38,
    fontWeight: 'bold',
    padding: 10,
    color: '#000AFF',
    textAlign: 'center',
    textShadowColor: 'rgba(0, 0, 0, 0.5)',
    textShadowOffset: { width: 2, height: 2 },
    textShadowRadius: 4,
  },
  roleCard: {
    marginVertical: 10,
    backgroundColor: 'white',
    opacity: 0.8,
    borderRadius: 30,
    width: '100%',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10,
    paddingBottom: 10,
    paddingHorizontal: 15,
  },
  textContent: {
    fontSize: 16,
    paddingLeft: 15,
    paddingTop: 5,
  },
  textPS: {
    fontSize: 12,
    paddingLeft: 15,
    color: '#FF0000',
  },
  imageCard: {
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 5,
  },
  buttonContainer: {
    backgroundColor: '#1876D0',
    borderRadius: 30,
    width: '70%',
    height: "8%",
    justifyContent: 'center',
    marginTop: 'auto',
    alignSelf: 'center',
  },
  texte: {
    color: 'white',
    fontSize: 26,
    textAlign: 'center',
  },
});

function RegleScreen({ navigation }) {
  const back = require('../assets/img/kiev.png');
  const icon = require('../assets/img/map.png');

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <ImageBackground source={back} resizeMode="cover" style={styles.imageBack} />
      <View style={styles.contentCard}>
        <View style={styles.titleCard}>
          <Text style={styles.title}>Règles du jeu</Text>
        </View>
        <ScrollView contentContainerStyle={styles.scrollViewContainer}>
        <View style={styles.roleCard}>
        <Text style={styles.textContent}><Text style={{ color: '#000AFF' }}>L'organisateur</Text> crée un nouveau jeu, où il indique des marqueurs avec des énigmes et un prix.</Text>
        <Text style={styles.textPS}>*Il peut y avoir un maximum de 10 points d'arrêt dans le jeu.</Text>
        <View style={styles.imageCard}>
          <Image source={icon}></Image>
        </View>
        <Text style={styles.textContent}><Text style={{ color: '#000AFF' }}>L'organisateur</Text> propose lui-même des énigmes sous la forme d'une question et propose au <Text style={{ color: '#000AFF' }}>joueur</Text> des options pour choisir une réponse.</Text>
        <Text style={styles.textPS}>*Les énigmes doivent être directement liées à l'emplacement du marqueur. Ils peuvent couvrir des sujets historiques, artistiques, etc.</Text>
        <View style={styles.imageCard}>
          <Image source={icon}></Image>
        </View>
        <Text style={styles.textContent}><Text style={{ color: '#000AFF' }}>Le joueur</Text> commence au point de départ où il résout la première énigme.</Text>
        <Text style={styles.textContent}>Si la réponse est correcte, <Text style={{ color: '#000AFF' }}>le joueur</Text> accède au prochain point d'énigme et bénéficie de temps supplémentaire.</Text>
        <Text style={styles.textContent}>Si la réponse est incorrecte, il perd du temps.</Text>
        <View style={styles.imageCard}>
          <Image source={icon}></Image>
        </View>
        <Text style={styles.textContent}>La durée du temps supplémentaire est déterminée par <Text style={{ color: '#000AFF' }}>l'organisateur</Text>.</Text>
        <Text style={styles.textContent}>Le nombre de pénalités et de prolongations ne devrait pas différer.</Text>
        <View style={styles.imageCard}>
          <Image source={icon}></Image>
        </View>
        <Text style={styles.textContent}>En cas de difficultés pour terminer la quête, <Text style={{ color: '#000AFF' }}>le joueur</Text> peut toujours contacter l'organisateur dans un chat privé.</Text>
        <View style={styles.imageCard}>
          <Image source={icon}></Image>
        </View>
        <Text style={styles.textContent}>Un certain temps est accordé pour terminer la quête, qui est déterminé par <Text style={{ color: '#000AFF' }}>l'organisateur</Text>. <Text style={{ color: '#000AFF' }}>Le joueur</Text> peut observer une minuterie du temps restant et des énigmes en haut de l'écran.</Text>
        <View style={styles.imageCard}>
          <Image source={icon}></Image>
        </View>
        <Text style={styles.textContent}>Le jeu est considéré comme terminé si <Text style={{ color: '#000AFF' }}>le joueur</Text> devine toutes les énigmes de <Text style={{ color: '#000AFF' }}>l'organisateur</Text> ou si le temps imparti est écoulé.</Text>
        </View>
        </ScrollView>
        <Pressable onPress={() => navigation.navigate('Role')} style={styles.buttonContainer}>
          <Text style={styles.texte}>Continuer</Text>
        </Pressable>
      </View>
    </ScrollView>
  );
}

export default RegleScreen;
