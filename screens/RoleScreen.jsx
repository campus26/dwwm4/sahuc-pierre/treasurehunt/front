import * as React from 'react';
import { StyleSheet, View, Text, Pressable, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { RadioButton } from 'react-native-paper';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const contentCardHeight = screenHeight / 1.2;
const contentCardWidth = screenWidth * 0.9;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  imageBack: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  contentCard: {
    position: 'absolute',
    width: contentCardWidth,
    height: "93%",
    top: '45%',
    marginTop: -contentCardHeight / 2,
    alignItems: 'center',
  },
  titleCard: {
    marginBottom: 50,
    backgroundColor: 'white',
    opacity: 0.8,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 38,
    fontWeight: 'bold',
    padding: 10,
    color: '#000AFF',
    textAlign: 'center',
    textShadowColor: 'rgba(0, 0, 0, 0.5)',
    textShadowOffset: { width: 2, height: 2 },
    textShadowRadius: 4,
  },
  roleCard: {
    marginVertical: 10,
    backgroundColor: 'white',
    opacity: 0.8,
    borderRadius: 30,
    width: '100%',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10, 
  },
  labelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 5,
  },
  subTitle: {
    textAlign: 'center',
    color: '#FF0000',
    fontSize: 26,
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
  textContent: {
    fontSize: 14,
    paddingLeft: 35,
  },
  buttonContainer: {
    marginTop: 20,
    backgroundColor: '#1876D0',
    borderRadius: 30,
    width: '70%',
    height: "8%",
    justifyContent: 'center',
    marginTop: 'auto',
    alignSelf: 'center',
  },
  texte: {
    color: 'white',
    fontSize: 26,
    textAlign: 'center',
  },
});

function RadioButtonWithLabel({ label, value, selectedValue, setSelectedValue }) {
  return (
    <TouchableOpacity style={styles.labelContainer} onPress={() => setSelectedValue(value)}>
      <RadioButton
        value={value}
        status={selectedValue === value ? 'checked' : 'unchecked'}
        onPress={() => setSelectedValue(value)}
        color="#000AFF"
        style={styles.radioButton}
      />
      <View style={{ flex: 1 }}>
        <Text style={styles.subTitle}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
}

function RoleScreen({ navigation }) {
  const back = require('../assets/img/kiev.png');
  const [selectedValue, setSelectedValue] = React.useState('Jouer');

  const handleContinuePress = () => {
    if (selectedValue === 'Organisateur') {
      navigation.navigate('MenuOrganisateur');
    } else if (selectedValue === 'Jouer') {
      navigation.navigate('Category');
    }
  };

  return (
    <View style={styles.container}>
      <ImageBackground source={back} resizeMode="cover" style={styles.imageBack} />
      <View style={styles.contentCard}>
        <View style={styles.titleCard}>
          <Text style={styles.title}>Choisissez votre rôle</Text>
        </View>
        <View style={styles.roleCard}>
        <RadioButtonWithLabel
          label="JOUER"
          value="Jouer"
          selectedValue={selectedValue}
          setSelectedValue={setSelectedValue}
        />
        <Text style={styles.textContent}>&#x25CF; Le joueur sélectionne la quête qu'il souhaite accomplir.</Text>
        <Text style={styles.textContent}>&#x25CF; Devinez les énigmes de l'organisateur et avancez sur la carte.</Text>
        <Text style={[styles.textContent, {paddingBottom: 10}]}>
          <Text>&#x25CF; </Text>
          <Text>La tâche du joueur est de résoudre toutes les énigmes et de trouver le trésor avant la fin du temps imparti.</Text>
        </Text>
        </View>
        <View style={styles.roleCard}>
        <RadioButtonWithLabel
          label="ORGANISATEUR"
          value="Organisateur"
          selectedValue={selectedValue}
          setSelectedValue={setSelectedValue}
        />
        <Text style={styles.textContent}>&#x25CF; Les organisateurs doivent proposer leurs propres énigmes avec des réponses possibles.</Text>
        <Text style={styles.textContent}>&#x25CF; Pour chaque énigme, vous devez placer un marqueur sur la carte.</Text>
        <Text style={[styles.textContent, {paddingBottom: 10}]}>
          <Text>&#x25CF; </Text>
          <Text>Si le joueur a des difficultés, aidez-le à trouver une issue.</Text>
        </Text>
        </View>
        <Pressable onPress={handleContinuePress} style={styles.buttonContainer}>
          <Text style={styles.texte}>Continuer</Text>
        </Pressable>
      </View>
    </View>
  );
}

export default RoleScreen;