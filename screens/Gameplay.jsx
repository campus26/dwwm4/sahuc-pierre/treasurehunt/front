import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Dimensions, Text, Modal, Button } from 'react-native';
import MapView, { Marker, UrlTile } from 'react-native-maps';
import Question from './Question';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const contentCardHeight = screenHeight / 1.2;
const contentCardWidth = screenWidth * 0.9;


function Gameplay({ navigation, route }) {
  const { penalty } = route.params;
  const [markerIndex, setMarkerIndex] = useState(0);
  const [markers, setMarkers] = useState([]);
  const [solvedCount, setSolvedCount] = useState(0);
  const [mapRegion, setMapRegion] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [currentTime, setCurrentTime] = useState(route.params.temps);
  const [gameFinished, setGameFinished] = useState(false); 

  useEffect(() => {
    if (route.params && route.params.marker && route.params.marker.length > 0) {
      setMarkers(route.params.marker);
      // setMarkerIndex(0);
      setSolvedCount(0);
      const { lat, lng } = route.params.marker[markerIndex];
      setMapRegion({
        latitude: lat,
        longitude: lng,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      });
    }
  }, [route.params]);
  
  function handleNextMarker() {
    setMarkerIndex(prevIndex => {
      if (prevIndex < markers.length - 1) {
        const { lat, lng } = markers[markerIndex];
        setMapRegion({
          latitude: lat,
          longitude: lng,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        });
        return prevIndex + 1;
      } else {
        return prevIndex;
      }
    });
  }
  
  function handleCorrectAnswer() {
    setSolvedCount(prevCount => prevCount + 1);
    if (markerIndex < markers.length - 1) {
      handleNextMarker();
    } else {
      setGameFinished(true);
      setShowModal(true);
    }
  }

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentTime(prevTime => {
        if (prevTime === 0) {
          setGameFinished(true);
          setShowModal(true);
          clearInterval(interval);
          return prevTime;
        } else {
          return prevTime - 1;
        }
      });
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  const remainingCount = markers.length - solvedCount;

  const hours = Math.floor(currentTime / 3600);
  const minutes = Math.floor((currentTime % 3600) / 60);
  const seconds = currentTime % 60;
  const formattedTime = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;

  return (
    <View style={styles.container}>
      <MapView mapType={"none"} style={styles.map} region={mapRegion}>
        <UrlTile
          urlTemplate="https://a.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png"
          maximumZ={19}
        />
        {markers.length > 0 && markerIndex <= markers.length - 1 && (
          <Marker
            coordinate={{ latitude: markers[markerIndex].lat, longitude: markers[markerIndex].lon }}
          />
        )}
      </MapView>
      <View style={styles.contentCard}>
        <View style={styles.titleCard}>
          <Text style={[styles.title, { paddingTop: 10 }]}>
            Nombre d'énigmes résolues :{" "}
            <Text style={{ color: '#FF0000', fontWeight: 'bold' }}>
              {solvedCount}
            </Text>
          </Text>
          <Text style={[styles.title, { paddingBottom: 10 }]}>
            Nombre d'énigmes non résolues :{" "}
            <Text style={{ color: '#FF0000', fontWeight: 'bold' }}>
              {remainingCount}
            </Text>
          </Text>
          <Text style={[styles.title, { paddingBottom: 10 }]}>
            Minuteur :{" "}
            <Text style={{ color: '#FF0000', fontWeight: 'bold' }}>
              {formattedTime}
            </Text>
          </Text>
        </View>
        {markers.length > 0 && markerIndex < markers.length && (
          <Question
          question={markers[markerIndex].question}
          reponse1={markers[markerIndex].reponse1}
          reponse2={markers[markerIndex].reponse2}
          reponse3={markers[markerIndex].reponse3}
          correctAnswer={markers[markerIndex].correctAnswer}
          penalty={penalty}
          onNextMarker={handleNextMarker}
          onCorrectAnswer={handleCorrectAnswer}
          setCurrentTime={setCurrentTime}
        />
        )}
      </View>
      <Modal
        visible={showModal}
        animationType="slide"
        transparent={true}
      >
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
          <View style={{ backgroundColor: 'white', padding: 20, borderRadius: 10, alignItems: 'center' }}>
            {gameFinished ? (
              <Text style={{ fontSize: 18, marginBottom: 10 }}>Toutes nos félicitations! Tous les marqueurs sont résolus !</Text>
            ) : (
              <Text style={{ fontSize: 18, marginBottom: 10 }}>Oh non! Temps est révolu!</Text>
            )}
            <Button
              title="Category"
              onPress={() => {
                setShowModal(false);
                navigation.navigate('Category');
              }}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
}

export default Gameplay;


const styles = StyleSheet.create({
  map: {
    width: '100%',
    height: '100%',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  contentCard: {
    position: 'absolute',
    width: contentCardWidth,
    height: "96%",
    top: '45%',
    marginTop: -contentCardHeight / 2,
    alignItems: 'center',
  },
  titleCard: {
    marginBottom: 50,
    backgroundColor: 'white',
    opacity: 0.8,
    borderRadius: 30,
    justifyContent: 'center',
    width: "100%",
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingLeft: 20,
    color: 'black',
    textShadowRadius: 4,
  },
});