
import { StyleSheet, Dimensions } from 'react-native';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const logoWidth = screenWidth / 2;
const logoHeight = logoWidth * 0.8;

const connectCardWidth = screenWidth * 0.7;
const connectCardHeight = screenHeight/2;

const buttonWidth = connectCardWidth*0.5;
const buttonHeight = connectCardHeight*0.12;

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center', 
        justifyContent: 'center',
    },

    imageBack: {
        flex:1,
        width: '100%',
        height: '100%',
    },

    containerLogo: {
        position: 'absolute',
        top: logoWidth/2,
        left: (screenWidth - logoWidth) / 2,
        zindex: 3,
    },

    logo: {
        width: logoWidth,
        height: logoHeight,
        alignItems: 'center',
        justifyContent: 'center',
    },

    connectCard: {
        position: 'absolute',
        bottom: connectCardHeight*0.2,
        left: (screenWidth - connectCardWidth) / 2,
        backgroundColor: 'white',
        opacity: 0.6,
        borderRadius: 10,
        padding: 10,
        width: connectCardWidth,
        height: connectCardHeight,
    },

    contentCard: {
        position: 'relative',
    },
   
    buttonRegister: {
        position: 'absolute',
        top: connectCardHeight*0.65,
        right: (connectCardWidth - buttonWidth - 20) / 2,
        width: buttonWidth,
        height: buttonHeight,
        backgroundColor: '#1876D0',
        borderRadius: 50,
        padding: 10,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonLogin: {
        position: 'absolute',
        top: connectCardHeight*0.8,
        right: (connectCardWidth - buttonWidth - 20) / 2,
        width: buttonWidth,
        height: buttonHeight,
        backgroundColor: '#1876D0',
        borderRadius: 50,
        padding: 10,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonMenuOrganisateur: {
        position: 'absolute',
        right: (screenWidth - buttonWidth*2) / 2,
        width: buttonWidth*2,
        height: buttonHeight,
        backgroundColor: '#1876D0',
        borderRadius: 50,
        padding: 10,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    titre: {
        fontWeight: '800',
        fontSize: 16,
        opacity: 1,
        textAlign: 'center',
    },
    textButton: {
        color: 'white',
        fontWeight: '800',
        fontSize: 16,
        opacity: 1
    },
    input: {
        backgroundColor: 'white',
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
    },
    textFooter: {
        fontWeight: '800',
        fontSize: 16,
        opacity: 1,
        textAlign: 'center',
    }
});


